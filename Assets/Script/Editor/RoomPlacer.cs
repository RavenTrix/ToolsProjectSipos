using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class RoomPlacer : EditorWindow
{
    //creo una tendina chiamata Tools dove poter aprire la finestra dedicata al RoomPlacer
    [MenuItem("Tools/RoomPlacer")] public static void OpenWindow() => GetWindow<RoomPlacer>();

    public GameObject selectedPrefab;                               //prefab selezionato
    List<GameObject> alreadyPlacedRooms = new List<GameObject>();   //lista di stanze gi� piazzate
    GameObject[] prefabs;                                           //array dei prefab presenti nelle cartelle
    GameObject[] objectsInScene;                                    //array di oggetti presenti in scena
    GameObject folder;                                              //empty object che far� da contenitore per le stanze

    Vector3 previewDoorPosition, placedDoorPosition;                //posizione della porta corrente della preview e della stanza piazzata pi� vicina
    Quaternion roomRot = Quaternion.identity;                       //rotazione della preview
    Vector3 drawPosition;                                           //posizione della preview

    public Material previewMat;                                     //materiale della preview

    Transform[] previewDoorsTransform;                              //array delle posizioni delle porte della preview

    [SerializeField] bool[] prefabsSelection;                       //array di booleane relative ai button di selezione delle stanze
    bool inRange;                                                   //se true la preview si avvicina alla porta, se false resta sotto il cursore
    bool nextToDoor;                                                //se true la preview si trova nelle condizioni di essere piazzata
    bool rangeVisibility;                                           //se true si vede il range di snap

    public float range = 40f;                                       //range entro il quale la stanza si pu� snappare
    SerializedObject so;                                            //creo il mio serialized object
    SerializedProperty rangeP;                                      //property relativa al range
    private void OnEnable()
    {
        //setto questo script come serialized object, serializzo le property del range e faccio partire il DuringSceneGUI all'apertura del tool
        so = new SerializedObject(this);
        rangeP = so.FindProperty("range");
        SceneView.duringSceneGui += DuringSceneGUI;

        //mi creo un array di path settato di default alla cartella contenente le stanze con una porta sola e vado a pescarmele per inserirle in un altro array di prefab
        string[] guids = AssetDatabase.FindAssets("t:prefab", new[] { "Assets/Prefabs/Rooms/OneDoor" });
        IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
        prefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();

        //creo un empty object chiamato RoomsFolder se non esiste gi� per contenere tutte le stanze che piazzer�
        FolderCreation();

        //mi assicuro che se l'array di booleane � vuoto o la sua quantit� di elementi non corrisponde a quella dei prefab nella cartella, si mettano in pari
        if (prefabsSelection == null || prefabsSelection.Length != prefabs.Length)
        {
            prefabsSelection = new bool[prefabs.Length];
        }
    }

    //alla chiusura del tool disattivo il DuringSceneGUI
    private void OnDisable()
    {
        SceneView.duringSceneGui -= DuringSceneGUI;
    }

    //in OnGUI faccio partire l'update relativo a questo script, setto lo stile e imposto l'inspector applicando di volta in volta le property modificate
    private void OnGUI()
    {
        so.Update();

        StyleSetting();

        if (so.ApplyModifiedProperties())
        {
            SceneView.RepaintAll();
        }
    }

    //in DuringSceneGUI vado a gestire la lista di stanze piazzate, i button in scena, la preview, la rotazione del prefab,
    //lo spawn del prefab e le posizioni delle porte del prefab in preview
    private void DuringSceneGUI(SceneView sceneView)
    {
        //cerco qualunque GameObject in scena, filtro le stanze e le aggiungo alla lista
        AddingRoomsToList();

        //conto quanti prefab ci sono nella cartella selezionata e mostro le loro preview nei button visibili in SceneView
        DrawGUI();

        //faccio partire un raycast dal cursore che va a colpire la superficie del plane e ci disegno la preview
        Event e = Event.current;
        Ray ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);
        if (e.type == EventType.Repaint)
        {
            if (Physics.Raycast(ray, out RaycastHit hit) && hit.collider != null)
            {
                DrawPreview(sceneView, hit);

                //disegno un disco che rappresenti il range di snap (per le stanze con porte allineabili)
                if (rangeVisibility)
                {
                    Handles.color = Color.magenta;
                    Handles.DrawWireDisc(hit.point, hit.normal, range);
                }
            }
        }

        //tenendo premuto il tasto shift e cliccando E, il prefab della preview ruota in senso orario di 90�
        if (e.keyCode == KeyCode.E && e.type == EventType.KeyDown && e.shift)
        {
            RotateRoom(1);
            e.Use();
        }

        //tenendo premuto il tasto shift e cliccando Q, il prefab della preview ruota in senso antiorario di 90�
        if (e.keyCode == KeyCode.Q && e.type == EventType.KeyDown && e.shift)
        {
            RotateRoom(-1);
            e.Use();
        }

        //cliccando con il tasto sinistro sul plane (solo se la preview si allinea ad una porta) spawna il prefab
        if (e.type == EventType.MouseDown && e.button == 0)
        {
            TrySpawnObject();
            e.Use();
        }

        //cerco i transform di tutti i children del prefab (pi� avanti tolgo il primo elemento per non prendere il transform del parent)
        if (selectedPrefab != null)
        {
            previewDoorsTransform = selectedPrefab.GetComponentsInChildren<Transform>();
        }
    }
    //in scena si vedr� un consiglio sulla parte centrale in alto, i pulsanti relativi alle stanze e il button di undo
    void DrawGUI()
    {
        Handles.BeginGUI();

        GUILayout.Label("Click on scene view and rotate rooms with shift + Q and shift + E");

        Rect rect = new Rect(20, 60, 230, 100);

        //prendo i prefab nella cartella selezionata, ne faccio una preview da visionare sui button
        for (int i = 0; i < prefabs.Length; i++)
        {
            GameObject prefab = prefabs[i];
            Texture icon = AssetPreview.GetAssetPreview(prefab);
            EditorGUI.BeginChangeCheck();
            prefabsSelection[i] = GUI.Button(rect, icon);
            if (prefabsSelection[i])
            {
                selectedPrefab = prefab;
                roomRot = selectedPrefab.transform.rotation;
            }
            rect.y += rect.height + 20;
        }

        //creo l'undo button, anche se funziona anche il ctrl + Z
        UndoButton();

        Handles.EndGUI();
    }


    //se il cursore si trova nel range e la porta della preview si trova vicina ad un'altra porta (alla quale si pu� collegare)
    //do la posizione snappata alla stanza, in caso contrario la preview rimarr� sotto il cursore
    void DrawPreview(SceneView sceneView, RaycastHit hit)
    {
        if (selectedPrefab != null)
        {
            if (nextToDoor && inRange)
            {
                drawPosition = placedDoorPosition + (hit.point - previewDoorPosition);
            }
            else
            {
                drawPosition = hit.point + hit.normal;
            }
            Matrix4x4 pointToWorldMtx = Matrix4x4.TRS(drawPosition, roomRot, Vector3.one);
            MeshFilter objMeshFilter = selectedPrefab.GetComponent<MeshFilter>();
            Mesh mesh = objMeshFilter.sharedMesh;
            Material material = objMeshFilter.GetComponent<MeshRenderer>().sharedMaterial;
            material.SetPass(0);

            //Cerco la stanza piazzata pi� vicina alla preview e controllo la sua distanza dalla preview per disegnare la preview stessa
            Graphics.DrawMesh(mesh, pointToWorldMtx, material, 0, sceneView.camera);
            GetClosestRoom(drawPosition, hit);
        }
    }

    //prendo la rotazione del prefab selezionato e la cambio in modo che ruoti di 90� orari/antiorari e setto la nuova rotazione del prefab
    void RotateRoom(int sign)
    {
        selectedPrefab.transform.eulerAngles = new Vector3(0, selectedPrefab.transform.eulerAngles.y + (90 * sign), 0);
        Quaternion newRotation = selectedPrefab.transform.rotation;
        roomRot = newRotation;
        Repaint();
    }

    //creo i button in inspector che gestiranno la quantit� di porte presenti nelle stanze
    void DoorNumButtons(string buttonName, string buttonPath)
    {
        GUILayout.Space(10);
        if (GUILayout.Button(buttonName, GUILayout.Height(60)))
        {
            SetPaths(buttonPath);
        }
    }

    //il pulsante di undo sar� visibile in scena, oltre che funzionare con il semplice ctrl + Z
    void UndoButton()
    {
        Rect rect = new Rect(20, 420, 230, 100);
        if (GUI.Button(rect, "Undo"))
        {
            Undo.PerformUndo();
        }
    }

    //setto il path della cartella dalla quale vado a prendere i prefab
    void SetPaths(string path)
    {
        string[] guids = AssetDatabase.FindAssets("t:prefab", new[] { path });
        IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
        prefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();
    }

    //setto lo stile delle scritte visibili in Inspector e in SceneView
    void StyleSetting()
    {
        GUIStyle style = GUI.skin.GetStyle("Label");
        style.fontStyle = FontStyle.Bold;
        style.normal.textColor = Color.black;
        style.hover.textColor = Color.black;
        style.fontSize = 20;
        style.alignment = TextAnchor.MiddleCenter;

        GUIStyle buttonStyle = GUI.skin.GetStyle("Button");
        buttonStyle.normal.textColor = Color.black;
        buttonStyle.hover.textColor = Color.black;
        buttonStyle.fontSize = 16;
        buttonStyle.alignment = TextAnchor.MiddleCenter;

        InspectorSetting();
    }

    //Creo uno spazio dedicato al propertyField del range e i button relativi alle cartelle di prefab presenti nel progetto
    void InspectorSetting()
    {
        GUILayout.Space(5);
        EditorGUI.DrawRect(EditorGUILayout.GetControlRect(false, 1f), Color.gray);
        GUILayout.Space(5);
        GUILayout.Label("Set the placement range");
        EditorGUI.DrawRect(EditorGUILayout.GetControlRect(false, 1f), Color.gray);
        GUILayout.Space(25);
        GUILayout.BeginHorizontal();
        GUILayout.Space(200);
        EditorGUIUtility.labelWidth = 60;
        EditorGUIUtility.fieldWidth = 60;

        if (GUILayout.Toggle(rangeVisibility, "Range Visibility"))
            rangeVisibility = true;
        else
            rangeVisibility = false;

        GUILayout.EndHorizontal();
        GUILayout.Space(15);
        GUILayout.BeginHorizontal();
        GUILayout.Space(200);
        EditorGUIUtility.labelWidth = 60;
        EditorGUIUtility.fieldWidth = 60;
        EditorGUILayout.PropertyField(rangeP);
        GUILayout.Space(200);
        GUILayout.EndHorizontal();
        GUILayout.Space(25);
        EditorGUI.DrawRect(EditorGUILayout.GetControlRect(false, 1f), Color.gray);
        GUILayout.Space(5);
        GUILayout.Label("Select rooms by the number of doors");
        GUILayout.Space(5);
        EditorGUI.DrawRect(EditorGUILayout.GetControlRect(false, 1f), Color.gray);

        DoorNumButtons("One Door", "Assets/Prefabs/Rooms/OneDoor");
        DoorNumButtons("Two Doors", "Assets/Prefabs/Rooms/TwoDoors");
        DoorNumButtons("Three Doors", "Assets/Prefabs/Rooms/ThreeDoors");
        DoorNumButtons("Four Doors", "Assets/Prefabs/Rooms/FourDoors");
    }

    //se la preview � nel range e si trova vicina ad una porta alla quale si pu� attaccare, posso spawnare il prefab e gestire il suo undo
    void TrySpawnObject()
    {
        if (nextToDoor && inRange)
        {
            Vector3 spawnPosition = (drawPosition);
            Quaternion spawnRotation = roomRot;

            GameObject spawnedPrefab = (GameObject)PrefabUtility.InstantiatePrefab(selectedPrefab, folder.transform);

            spawnedPrefab.transform.position = spawnPosition;
            spawnedPrefab.transform.rotation = spawnRotation;
            Undo.RegisterCreatedObjectUndo(spawnedPrefab, "Placed Room");
        }
    }

    //se non esiste gi�, creo un empty object chiamato RoomsFolder che conterr� le stanze piazzate
    void FolderCreation()
    {
        var f = GameObject.Find("RoomsFolder");

        if (f != null)
        {
            folder = f;
        }

        if (folder == null)
        {
            folder = new GameObject();
            folder.name = "RoomsFolder";
        }
    }

    //cerco tutti gli oggetti in scena, se gli oggetti hanno il layer Room, vengono inseriti in una lista di stanze
    void AddingRoomsToList()
    {
        objectsInScene = FindObjectsOfType<GameObject>();

        for (int i = 0; i < objectsInScene.Length; i++)
        {
            if (objectsInScene[i] != null && objectsInScene[i].layer == 7)
            {
                if (!alreadyPlacedRooms.Contains(objectsInScene[i]))
                {
                    alreadyPlacedRooms.Add(objectsInScene[i]);
                }
            }
            else
                alreadyPlacedRooms.Remove(objectsInScene[i]);
        }
        for (int j = 0; j < alreadyPlacedRooms.Count; j++)
        {
            if (alreadyPlacedRooms[j] == null)
                alreadyPlacedRooms.RemoveAt(j);
        }

    }

    //controllo la lista di stanze in scena, se non � stata ancora settata quella pi� vicina, prende la prima presa in esame, senn� controlla le distanze
    //delle stanze piazzate e la preview, per aggiornare la stanza pi� vicina
    void GetClosestRoom(Vector3 position, RaycastHit hit)
    {
        GameObject closest = null;
        for (int i = 0; i < alreadyPlacedRooms.Count; i++)
        {
            GameObject targetRoom = alreadyPlacedRooms[i];
            if (closest == null)
            {
                closest = targetRoom;
            }
            else
            {
                if (CheckRoomsDistance(targetRoom, position) < CheckRoomsDistance(closest, position))
                {
                    closest = targetRoom;
                }
            }
        }

        //controllo la porta pi� vicina relativa alla stanza pi� vicina
        GetClosestDoor(closest, hit);
    }

    //controllo la distanza tra le stanze
    float CheckRoomsDistance(GameObject target, Vector3 pos)
    {
        float distance = (target.transform.position - pos).magnitude;
        return distance;
    }


    //cerco le porte relative alla stanza pi� vicina, controllo la distanza tra le porte
    //e infine controllo la direzione della porta piazzata presa in esame rispetto a quella della preview presa in esame
    void GetClosestDoor(GameObject closest, RaycastHit hit)
    {
        if (closest != null && previewDoorsTransform != null)
        {
            Transform closestD = null;
            Transform closestP = null;
            Transform[] doors = closest.GetComponentsInChildren<Transform>();

            //ignoro il transform del parent sia per le porte del closest che per le porte della preview
            for (int i = 1; i < doors.Length; i++)
            {
                for (int j = 1; j < previewDoorsTransform.Length; j++)
                {
                    Vector3 previewDoors = previewDoorsTransform[j].position + hit.point;

                    //cerco la closest door tra quelle della stanza piazzata
                    if (closestD == null)
                    {
                        closestD = doors[i];
                    }
                    else
                    {
                        if (CheckPlacedDoorsDistance(doors[i].gameObject, previewDoors) < CheckPlacedDoorsDistance(closestD.gameObject, previewDoors))
                        {
                            closestD = doors[i];
                        }
                    }
                    placedDoorPosition = closestD.position;

                    //cerco la closest door tra quelle della stanza in preview
                    if (closestP == null)
                    {
                        closestP = previewDoorsTransform[j];
                    }
                    else
                    {
                        if (CheckPreviewDoorsDistance(previewDoorsTransform[j].gameObject, closestD.position,hit) < CheckPreviewDoorsDistance(closestP.gameObject, closestD.position,hit))
                        {
                            closestP = previewDoorsTransform[j];
                        }
                    }

                    //controllo la direzione tra la porta "piazzata" pi� vicina e la porta "preview" pi� vicina
                    CheckDoorsDirection(closestD, closestP, hit);
                }
            }
        }
    }

    //controllo la distanza tra le porte "piazzate" e tutte quelle della "preview" e setto la booleana inRange
    float CheckPlacedDoorsDistance(GameObject target, Vector3 pos)
    {
        float distance = (target.transform.position - pos).magnitude;
        if (distance < range)
            inRange = true;
        else
            inRange = false;
        return distance;
    }

    //controllo la distanza tra la porta "piazzata" pi� vicina e tutte quelle della "preview"
    float CheckPreviewDoorsDistance(GameObject previewTarget, Vector3 closestPlacedPos, RaycastHit hit)
    {
        float distance = ((previewTarget.transform.position+hit.point) - closestPlacedPos).magnitude;
        return distance;
    }

    //controllo con il dot product se le porte hanno direzioni opposte e se � cos�, la stanza pu� essere collegata
    void CheckDoorsDirection(Transform closestDoor, Transform previewDoor, RaycastHit hit)
    {
        Vector3 closestDirection = closestDoor.forward;
        Vector3 previewDirection = previewDoor.forward;

        int dotProd = (int)Vector3.Dot(closestDirection, previewDirection);
        if (dotProd == -1)
        {
            previewDoorPosition = previewDoor.position + hit.point;
            nextToDoor = true;
        }
        else
            nextToDoor = false;
    }
}