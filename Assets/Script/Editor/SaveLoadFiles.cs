using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

//creo una classe di dati da salvare
public class DataToSave
{
    public GameObject rooms;
    public GameObject centralRoom;
}

//come per il tool RoomPlacer, creo una tendina menu tra i Tools chiamata SaveLoadFiles dalla quale si pu� aprire il tool di salvataggio
public class SaveLoadFiles : EditorWindow
{
    [MenuItem("Tools/SaveLoadFiles")] public static void OpenWindow() => GetWindow<SaveLoadFiles>();

    DataToSave dataToSave;      //la classe dalla quale posso accedere alle variabili che mi servono
    string saveFilePath;        //la stringa che conterr� il path dove salvare i files
    GameObject folder;          //empty object che conterr� il livello salvato

    SerializedObject so;        //il serialized object di questo script
    bool canLoad;               //se true posso caricare le stanze salvate
    private void OnEnable()
    {
        so = new SerializedObject(this);                //setto questo script come serialized object

        SceneView.duringSceneGui += DuringSceneGUI;     //faccio partire il DuringSceneGUI all'apertura del tool
    }

    //alla chiusura del tool disattivo il DuringSceneGUI
    private void OnDisable()
    {
        SceneView.duringSceneGui -= DuringSceneGUI;
    }

    //in OnGUI faccio partire l'update relativo a questo tool e setto l'inspector
    private void OnGUI()
    {
        so.Update();
        InspectorSetting();
    }

    //in DuringSceneGUI cerco un oggetto in scena chiamato RoomsFolder e lo setto come insieme di stanze da salvare
    //cerco la CentralRoom e la setto come stanza centrale da salvare e se c'� qualcosa da caricare in scena, spawno le stanze
    private void DuringSceneGUI(SceneView sceneView)
    {
        dataToSave = new DataToSave();
        dataToSave.rooms = GameObject.Find("RoomsFolder");
        dataToSave.centralRoom = GameObject.Find("CentralRoom");
        if (canLoad && dataToSave.rooms != null)
        {
            InstantiateRooms();
        }

        //mi setto il path dove salvare le mie stanze
        saveFilePath = Application.persistentDataPath + "/DataToSave.json";
    }

    //istanzio le stanze dando loro un parent, posizione e rotazione in scena, in modo che siano allineate al centro
    void InstantiateRooms()
    {
        FolderCreation();
        GameObject rooms = Instantiate(dataToSave.rooms, folder.transform);
        rooms.transform.position = Vector3.zero;
        rooms.transform.rotation = Quaternion.identity;
        GameObject centralR = Instantiate(dataToSave.centralRoom, folder.transform);
        centralR.transform.position = dataToSave.centralRoom.transform.position;
        centralR.transform.rotation = Quaternion.identity;
        canLoad = false;
    }

    //se non esiste gi�, creo un empty object che conterr� il livello salvato
    void FolderCreation()
    {
        var f = GameObject.Find("SavedLevel");

        if (f != null)
        {
            folder = f;
        }

        if (folder == null)
        {
            folder = new GameObject();
            folder.name = "SavedLevel";
        }
    }

    //setto l'inspector, i suoi button e i suoi textField
    void InspectorSetting()
    {
        GUILayout.Space(15);
        SaveFile();
        LoadFile();
        DeleteFile();
    }

    //il primo button salver� i gameObject della stanza centrale e di quelle attaccate ad essa al path deciso in precedenza
    void SaveFile()
    {
        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save", GUILayout.Width(100), GUILayout.Height(40)))
        {
            string saveData = JsonUtility.ToJson(dataToSave);
            File.WriteAllText(saveFilePath, saveData);

            Debug.Log("File created at: " + saveFilePath);
        }
        GUILayout.TextField(saveFilePath, GUILayout.Height(40)); ;
        GUILayout.EndHorizontal();
    }

    //il secondo button caricher� il salvataggio fatto portando allo spawn del livello in scena
    void LoadFile()
    {
        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Load", GUILayout.Width(100), GUILayout.Height(40)))
        {
            if (File.Exists(saveFilePath))
            {
                string loadData = File.ReadAllText(saveFilePath);
                dataToSave = JsonUtility.FromJson<DataToSave>(loadData);
                if (dataToSave.rooms != null && dataToSave.centralRoom != null)
                {
                    canLoad = true;
                }
                Debug.Log("Loading completed!");
            }
            else
            {
                canLoad = false;
                Debug.Log("There is no save files to load!");
            }
        }
        GUILayout.TextField(saveFilePath, GUILayout.Height(40));
        GUILayout.EndHorizontal();
    }

    //il terzo button cancella qualunque salvataggio fatto in precedenza al path deciso in precedenza
    void DeleteFile()
    {
        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Delete", GUILayout.Width(100), GUILayout.Height(40)))
        {
            if (File.Exists(saveFilePath))
            {
                File.Delete(saveFilePath);
                Debug.Log("File deleted");
            }
            else
                Debug.Log("There is nothing to delete!");
        }
        GUILayout.TextField(saveFilePath, GUILayout.Height(40));
        GUILayout.EndHorizontal();
    }
}
