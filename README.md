# ToolsProjectSipos

Questo progetto presenta due tool con funzioni diverse:

- Il RoomPlacer che permette di scegliere le stanze da piazzare e che gestisce lo snap, la rotazione e lo spawn
della stanza scelta

- Il SaveLoadFiles che permette di salvare, caricare in scena ed eliminare il livello creato

Entrambi i tool si trovano nella tendina apposita in alto a sinistra chiamata Tools

Consiglio: dockare in alto a destra il RoomPlacer e in basso a destra il SaveLoadFiles per avere un controllo
maggiore del resto del layout

In scena � essenziale che non venga eliminata la stanza centrale perch� altrimenti � impossibile spawnare altre
stanze o salvare il livello

Altra cosa che non va eliminata � il plane che fa da pavimento, o non sar� possibile spawnare la stanza all'altezza
giusta


## RoomPlacer

Una volta dockata la window del tool � possibile vedervi:

- Un toggle relativo al range: se true in scena � visibile un cerchio magenta entro il quale avviene lo snap

- Un float che gestisce la portata del range e quindi da che distanza pu� avvenire lo snap

- Quattro pulsanti, ognuno che prende una cartella diversa di prefab di stanze in base al numero di porte

In scena, invece, si vedono tre pulsanti che riportano la preview delle tre shape di stanze e un button di Undo

Per poter effettivamente usufruire del tool � necessario selezionare la scena (preferibilmente cliccandoci sopra
con il tasto destro del mouse, per evitare di piazzare una stanza per sbaglio)

Per vedere la preview della stanza desiderata sotto il cursore � necessario selezionare la stanza dai button in scena

Una volta selezionata la stanza sar� possibile vedere la preview scorrere in scena sul plane finch� non ci si avvicina
alla stanza centrale, dove snapper� verso la porta pi� vicina con verso opposto

Se si desidera ruotare la stanza basta tenere premuto shift e cliccare il tasto Q o il tasto E per ruotarla di 90�

ATTENZIONE: potrebbe sparire temporaneamente l'immagine della preview sui button quando si usa la shortcut ctrl + S
se la stanza � stata ruotata, poich� viene ruotato anche il prefab relativo a quella stanza (che dovr� riaggiornarsi
con un altro ctrl + S)

Per piazzare la stanza baster� cliccare con il tasto sinistro del mouse nella zona scelta (purch� la stanza sia
allineata alla porta di un'altra)

Se si desidera annullare un'azione baster� usare la shortcut ctrl + Z o cliccare il tasto Undo


## SaveLoadFiles

Il tool di salvataggio del livello mostra solo tre button e i tre path di salvataggio relativi al file:

- da salvare

- da caricare

- da cancellare



Sar� possibile salvare un livello sin dall'apertura del tool fintanto che esiste la stanza centrale e un empty
object chiamato RoomsFolder

Il livello si salver� al path visibile accanto al button chiamato Save



Per caricare un livello salvato in precedenza baster� cliccare su Load e la stanza centrale + quelle ad essa
collegate spawneranno in scena nel punto d'origine del mondo

ATTENZIONE: se lo si fa nella stessa scena dove � stata creato il livello, spawner� nella stessa posizione
di quello gi� presente, perci� consiglio di chiudere il tool RoomPlacer, spostare il livello caricato altrove
e se si vuole proseguire con l'assemblaggio, riaprire in seguito il tool

Sar� possibile caricare un livello solo se � gi� stato salvato, altrimenti verr� fuori una linea di debug
che ricorda di salvare prima di caricare un livello



Una volta completato un salvataggio sar� possibile eliminarlo direttamente cliccando sul tasto Delete

Sar� possibile eliminare un salvataggio anche dopo averlo caricato in scena (non si eliminer� dalla scena,
solo dalla cartella al path visibile accanto al tasto Delete)